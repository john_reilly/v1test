var assert = require('assert');
var hello = require('../handler.js');

describe('Hello1', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {

      var expected = "Hello Serverless!";
      var actual = JSON.parse(hello.hello().body).message;
      assert.equal(actual, expected);


    });
  });
});
