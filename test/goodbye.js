var assert = require('assert');
var goodbye = require('../handler.js');

describe('Goodbye1', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {

      var expected = "Goodbye Serverless!";
      var actual = JSON.parse(goodbye.goodbye().body).message;
      assert.equal(actual, expected);

    });
  });
});
